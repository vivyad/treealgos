
# Tree Data Structure Implementation

- This project contains the implementation of tree algorithms in java.
- Compatible version: JDK 8
- Interface: [Tree.java](src/main/java/algos/tree/Tree.java)
- Implementations: [AbstractTree.java](src/main/java/algos/tree/AbstractTree.java), [TreeImpl.java](src/main/java/algos/tree/TreeImpl.java) and [TreeImplContainsString.java](src/main/java/algos/tree/TreeImplContainsString.java)
- Tests:
    - [AbstractTreeTest.java](src/test/java/algos/tree/AbstractTreeTest.java)
    - [TreeImplTestInt.java](src/test/java/algos/tree/TreeImplTestInt.java)
    - [TreeImplTestList.java](src/test/java/algos/tree/TreeImplTestList.java)
    - [TreeImplTestString.java](src/test/java/algos/tree/TreeImplTestString.java)


