package algos.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of {@link Tree}.
 * 
 * @author Vivek
 *
 * @param <E>
 * 
 * @created 14 Aug 2020
 */
public abstract class AbstractTree<E> implements Tree<E> {

	public static final String TREE_SYMBOL = "-<";

	private E value;

	@SuppressWarnings("unchecked")
	private Tree<E>[] branches = new Tree[0];

	/**
	 * Creates tree object with the given <code>name</code> and optional
	 * <code>branches</code> parameter.
	 * 
	 * @param value    {@link E} assigned to the tree.
	 * @param branches {@link Tree[]} assigned to the tree.
	 */
	@SafeVarargs
	public AbstractTree(final E value, Tree<E>... branches) {
		Tree.super.checkValue(value);
		this.value = value;
		// Add given leaves
		if (branches != null && branches.length > 0) {
			for (Tree<E> branch : branches) {
				this.addBranch(branch);
			}
		}
	}

	@Override
	public void setValue(E value) {
		Tree.super.checkValue(value);
		if (!this.hasTreeWithValue(value)) {
			this.value = value;
		}
	}

	@Override
	public E getValue() {
		return this.value;
	}

	@Override
	public void addBranch(final Tree<E> leaf) {
		if (leaf == null) {
			throw new NullPointerException("Leaf cannot be null!");
		}
		if (this == leaf) {
			throw new IllegalArgumentException("Tree cannot be assigned as a leaf to itself.");
		}
		if (!this.hasTreeWithValue(leaf.getValue())) {
			putBranch(leaf);
		}
	}

	private void putBranch(final Tree<E> leaf) {
		final int len = branches.length;
		this.branches = Arrays.copyOf(branches, len + 1);
		this.branches[len] = leaf;
	}

	@Override
	public Tree<E>[] getBranches() {
		return this.branches;
	}

	@Override
	public Tree<E> getBranchByValue(E value) {
		if (value == null || this.getBranches().length == 0) {
			return null;
		}
		for (Tree<E> tree : this.getBranches()) {
			if (tree.getValue().equals(value)) {
				return tree;
			}
		}
		return null;
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean hasTreeWithValue(final E value) {
		// No need to add additional null checks
		if (this.value.equals(value)) {
			return true;
		}
		// Has more branches
		for (Tree t : this.getBranches()) {
			if (t.hasTreeWithValue(value)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public List<List<E>> treeValueList() {
		final List<List<E>> listTrees = new ArrayList<>();
		AbstractTree.treeValueList(this, listTrees, new ArrayList<>());
		return listTrees;
	}

	/**
	 * Returns the representation of the given {@link Tree} with branches in
	 * {@link List}. <br/>
	 * <br/>
	 * Example Tree with 'A' as top most node:<br/>
	 * ... A........<br/>
	 * .../.\.......<br/>
	 * ..B...C......<br/>
	 * ...../\ \....<br/>
	 * ....D..E.F...<br/>
	 * <br/>
	 * Output <code>listTrees</code>: List[ A->B , A->C->D , A->C->E , A->C->F ]
	 * 
	 * @param tree       {@link Tree} given
	 * @param listTrees  {@link List} containing branches of the tree from top to
	 *                   bottom without split
	 * @param listParent {@link List} is upper branch in the tree structure
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void treeValueList(final Tree<?> tree, final List listTrees, final List<Tree<?>> listParent) {
		// No need to add additional null checks
		List<Tree<?>> list = new ArrayList<>(listParent);
		list.add(tree);
		// End of branches
		if (tree.getBranches().length == 0) {
			final List<Object> valueList = new ArrayList<>(list.size());
			list.forEach(e -> valueList.add(e.getValue()));
			listTrees.add(valueList);
			return;
		}
		// Has more branches
		for (Tree t : tree.getBranches()) {
			AbstractTree.treeValueList(t, listTrees, list);
		}
	}

	@Override
	public Iterator<E> iterator() {
		final List<E> listValues = new ArrayList<>();
		AbstractTree.valueList(this, listValues);
		return listValues.iterator();
	}

	/**
	 * Returns an list containing elements of type {@code E}. <br/>
	 * <br/>
	 * Example Tree with 'A' as top most node:<br/>
	 * ... A........<br/>
	 * .../.\.......<br/>
	 * ..B...C......<br/>
	 * ...../\ \....<br/>
	 * ....D..E.F...<br/>
	 * <br/>
	 * Output <code>list</code>: A, B, C, D, E, F
	 * 
	 * @return list {@link List}
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void valueList(final Tree<?> tree, final List listTrees) {
		// No need to add additional null checks
		listTrees.add(tree.getValue());
		// Has more branches
		for (Tree t : tree.getBranches()) {
			AbstractTree.valueList(t, listTrees);
		}
	}

}
