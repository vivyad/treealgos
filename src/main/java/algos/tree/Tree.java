package algos.tree;

import java.util.List;

/**
 * Data structure to store data in tree format.
 * 
 * @author Vivek
 * 
 * @created 08 Aug 2020
 */
public interface Tree<E> extends Iterable<E> {

	/**
	 * Assigns the given <code>value</code> to this tree.
	 * 
	 * @param value {@link E} to assign
	 */
	default boolean checkValue(E value) {
		if (value == null) {
			throw new NullPointerException("Tree name cannot be null!");
		}
		if ("".equals(value.toString().trim())) {
			throw new IllegalArgumentException("Tree name cannot be blank!");
		}
		// Call your value setter method after this.
		return true;
	}

	/**
	 * Assigns the given <code>value</code> to this tree.
	 * 
	 * @param value {@link E} to assign
	 */
	void setValue(E value);

	/**
	 * Returns value assigned to this tree.
	 * 
	 * @return value {@link E} assigned
	 */
	E getValue();

	/**
	 * Adds the given <code>branch</code> to this tree.
	 * 
	 * @param branch {@link Tree} added to the current tree
	 */
	void addBranch(Tree<E> branch);

	/**
	 * Returns copy of all the branches from the given tree.
	 * 
	 * @return arrayOfBranch {@link Tree[]}
	 */
	Tree<E>[] getBranches();

	/**
	 * Returns the instance of tree with given <code>value</code> from this tree.
	 * 
	 * @return branch {@link Tree} with the given value
	 */
	Tree<E> getBranchByValue(E value);

	/**
	 * 
	 * @param value
	 * @return
	 */
	boolean hasTreeWithValue(E value);

	/**
	 * Returns the representation of the given {@link Tree} with branches in
	 * {@link List}. <br/>
	 * <br/>
	 * Example Tree:<br/>
	 * ... A........<br/>
	 * .../.\.......<br/>
	 * ..B...C......<br/>
	 * ...../\ \....<br/>
	 * ....D..E.F...<br/>
	 * <br/>
	 * Output <code>listTrees</code>: <br/>
	 * List[ [A->B] , [A->C->D] , [A->C->E] , [A->C->F] ]
	 * 
	 * @return treeValue {@link List} containing branches of the tree from top to
	 *         bottom
	 */
	List<List<E>> treeValueList();

	/**
	 * Check if the given input candidate {@link Tree} is present in (or part of)
	 * this data tree.
	 * 
	 * @param searchTree {@link Tree} candidate
	 * 
	 * @return flag {@link Boolean#booleanValue()} indicating presence of tree
	 */
	boolean containsTree(Tree<E> searchTree);

}
