package algos.tree;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of {@link Tree}.
 * 
 * @author Vivek
 *
 * @param <E>
 * 
 * @created 14 Aug 2020
 */
public class TreeImpl<E> extends AbstractTree<E> {

	/**
	 * See {@link AbstractTree#AbstractTree(Object, Tree...)}
	 * 
	 * @param value
	 * @param branches
	 */
	@SafeVarargs
	public TreeImpl(final E value, final Tree<E>... branches) {
		super(value, branches);
	}

	@Override
	public boolean containsTree(final Tree<E> searchTree) {
		if (searchTree == null) {
			return false;
		}
		if (this == searchTree) {
			return true;
		}

		final List<List<E>> treeListContain = this.treeValueList();
		final List<List<E>> treeListSearch = searchTree.treeValueList();

		if (treeListSearch.size() > treeListContain.size()) {
			return false;
		}

		int matches = 0;
		final Set<List<E>> matched = new HashSet<>();

		for (final List<E> listSearch : treeListSearch) {
			boolean isPresent = false;
			for (final List<E> listContain : treeListContain) {
				isPresent = this.matchElement(listSearch, listContain, matched);
				if (isPresent) {
					break;
				}
			}
			if (isPresent) {
				matches++;
			} else {
				break;
			}
		}
		return matches == treeListSearch.size();
	}

	/**
	 * Performs matching of the all the elements from <code>listSearch</code> with
	 * all the elements from <code>listContain</code>. If values of all the elements 
	 * from <code>listSearch</code> match with the values of some or all the elements
	 * of <code>listContain</code> then match flag is <code>true</code> otherwise
	 * <code>false</code>.
	 * 
	 * @param listSearch  {@link List} to match with <code>listContain</code>
	 * @param listContain {@link List} to be matched
	 * @param matched     {@link Set} contains already positively matched list
	 * @return match {@link Boolean#booleanValue()} found
	 */
	private boolean matchElement(final List<E> listSearch, final List<E> listContain, final Set<List<E>> matched) {
		final int searchLen = listSearch.size();
		final int containLen = listContain.size();
		if (matched.contains(listContain) || searchLen > containLen) {
			return false;
		}
		int si = 0;
		int ci = 0;
		while (searchLen > si && containLen > ci) {
			final E search = listSearch.get(si);
			final E contain = listContain.get(ci);
			if (search.equals(contain)) {
				si++;
				ci++;
			} else {
				ci++;
			}
		}
		if (searchLen == si) {
			matched.add(listContain);
			return true;
		}
		return false;
	}

}
