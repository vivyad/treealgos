package algos.tree;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of {@link Tree}.
 * 
 * @author Vivek
 *
 * @param <E>
 * 
 * @created 10 Aug 2020
 * @modified 14 Aug 2020
 */
public class TreeImplContainsString<E> extends AbstractTree<E> {

	/**
	 * See {@link AbstractTree#AbstractTree(Object, Tree...)}
	 * @param value
	 * @param branches
	 */
	@SafeVarargs
	public TreeImplContainsString(final E value, Tree<E>... branches) {
		super(value, branches);
	}

	@Override
	public boolean containsTree(final Tree<E> searchTree) {
		if (searchTree == null) {
			return false;
		}
		if (this == searchTree) {
			return true;
		}

		final List<List<E>> treeList = this.treeValueList();
		final List<List<E>> treeListSearch = searchTree.treeValueList();

		if (treeListSearch.size() > treeList.size()) {
			return false;
		}

		boolean contains = true;

		// Check if all branches of the candidate search tree are present in this tree
		final String strTreeList = treeList.toString();

		for (List<E> listSearch : treeListSearch) {
			final String strSearch = listSearch.toString();
			final String candidateString = strSearch.substring(1, strSearch.length() - 1);
			final int index = strTreeList.indexOf(candidateString);
			if (index > 1) {
				final String compareString = strTreeList.substring(index - 1, index + candidateString.length() + 1);
				if (!compareString.equals(strSearch) && !this.containsThoroughly(compareString, candidateString)) {
					contains = false;
				}
			} else {
				contains = false;
			}
			if (!contains) {
				break;
			}
		}
		return contains;
	}

	private boolean containsThoroughly(final String compareString, final String candidateString) {
		Set<String> candidates = new HashSet<>(4);
		candidates.add(String.format("[%s]", candidateString));
		candidates.add(String.format("[%s,", candidateString));
		candidates.add(String.format(" %s,", candidateString));
		candidates.add(String.format(" %s]", candidateString));
		return candidates.contains(compareString);
	}

}
