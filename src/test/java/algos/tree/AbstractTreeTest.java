package algos.tree;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

public class AbstractTreeTest {

	@Test
	public void testTreeConstrutorString() {
		final Tree<String> tree = new TreeImplInner<>("A");
		assertNotNull("Tree(E) => Object should not be null.", tree);
		assertEquals("Tree(E) => Tree name must be 'A'.", "A", tree.getValue());
	}

	@Test
	public void testTreeConstrutorStringNull() {
		try {
			new TreeImplInner<String>(null);
			fail("Tree(E) => Assertion fails upon reaching this program execution point.");
		} catch (NullPointerException ex) {
			assertEquals("Tree(E) => Null pointer exception message must match with the expected message.",
					"Tree name cannot be null!", ex.getMessage());
		}
	}

	@Test
	public void testTreeConstrutorStringBlank() {
		try {
			new TreeImplInner<String>(" ");
			fail("Tree(E) => Assertion fails upon reaching this program execution point.");
		} catch (IllegalArgumentException ex) {
			assertEquals("Tree(E) => Illegal argument exception message must match with the expected message.",
					"Tree name cannot be blank!", ex.getMessage());
		}
	}

	@Test
	public void testTreeConstrutorStringTrees() {
		final Tree<String> tree = new TreeImplInner<>("A", new TreeImplInner<>("B"), new TreeImplInner<>("C"));
		assertNotNull("Tree(E,Tree...) => Object should not be null.", tree);
		assertEquals("Tree(E,Tree...) => Tree name must be 'A'.", "A", tree.getValue());
		assertEquals("Tree(E,Tree...) => Tree should have 2 leaves.", 2, tree.getBranches().length);
		assertEquals("Tree(E,Tree...) => Tree's 1st leaf should be a tree with name 'B'.", "B",
				tree.getBranchByValue("B").getValue());
		assertEquals("Tree(E,Tree...) => Tree's 2nd leaf should be a tree with name 'C'.", "C",
				tree.getBranchByValue("C").getValue());
	}

	@Test
	public void testTreeConstrutorNullBranch() {
		assertEquals("Tree(E) => Null is not added to the branch.", 0,
				new TreeImplInner<>("A", null).getBranches().length);
	}

	@Test
	public void testTreeConstrutorEmptyBranch() {
		assertEquals("Tree(E) => Null is not added to the branch.", 0,
				new TreeImplInner<>("A", new Tree[] {}).getBranches().length);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testTreeConstrutorStringTreesNull() {
		final Tree[] leaves = { null };
		try {
			new TreeImplInner<String>("A", leaves);
			fail("Tree(E,Tree...) => Assertion fails upon reaching this program execution point.");
		} catch (NullPointerException ex) {
			assertEquals("Tree(E,Tree...) => Null pointer exception message must match with the expected message.",
					"Leaf cannot be null!", ex.getMessage());
		}
	}

	@Test
	public void testSetValueString() {
		final Tree<String> treeB = new TreeImplInner<>("A");
		treeB.setValue("B");
		assertEquals("Tree#setValue(E) => Tree name must be 'B'.", "B", treeB.getValue());
		final Tree<String> treeA = new TreeImplInner<>("C");
		assertEquals("Tree#setValue(E) => Tree name must be 'C'.", "C", treeA.getValue());
	}

	@Test
	public void testSetValueStringNull() {
		final Tree<String> tree = new TreeImplInner<>("A");
		try {
			tree.setValue(null);
			fail("Tree#setValue(E) => Assertion fails upon reaching this program execution point.");
		} catch (NullPointerException ex) {
			assertEquals(
					"Tree#setValue(E) => Null pointer exception message must match with the expected message.",
					"Tree name cannot be null!", ex.getMessage());
		}
	}

	@Test
	public void testSetValueStringBlank() {
		final Tree<String> tree = new TreeImplInner<>("A");
		try {
			tree.setValue(" ");
			fail("Tree#setValue(E) => Assertion fails upon reaching this program execution point.");
		} catch (IllegalArgumentException ex) {
			assertEquals(
					"Tree#setValue(E) => Illegal argument exception message must match with the expected message.",
					"Tree name cannot be blank!", ex.getMessage());
		}
	}

	@Test
	public void testAddBranch() {
		final Tree<String> tree = new TreeImplInner<>("A", new TreeImplInner<>("B"));
		tree.addBranch(new TreeImplInner<String>("C"));
		assertEquals("Tree#getBranches() => Tree should have 2 leaves.", 2, tree.getBranches().length);
		assertEquals("Tree#getBranchByValue(E) => Tree's 1st branch should have value 'B'.", "B",
				tree.getBranchByValue("B").getValue());
		assertEquals("Tree#getBranchByValue(E) => Tree's 2nd branch should have value 'C'.", "C",
				tree.getBranchByValue("C").getValue());
	}

	@Test
	public void testAddBranchNull() {
		final Tree<String> treeA = new TreeImplInner<>("A");
		try {
			treeA.addBranch(null);
			fail("Tree#getBranches() => Assertion fails upon reaching this program execution point.");
		} catch (NullPointerException ex) {
			assertEquals("Tree#getBranches() => Null pointer exception message must match with the expected message.",
					"Leaf cannot be null!", ex.getMessage());
		}
	}

	@Test
	public void testAddBranchSelfAssignment() {
		final Tree<String> treeB = new TreeImplInner<>("B");
		try {
			treeB.addBranch(treeB);
			fail("Tree#getBranches() => Assertion fails upon reaching this program execution point.");
		} catch (IllegalArgumentException ex) {
			assertEquals(
					"Tree#getBranches() => Illegal argument exception message must match with the expected message.",
					"Tree cannot be assigned as a leaf to itself.", ex.getMessage());
		}
	}

	@Test
	public void testSetValueWithBranchContainingValue() {
		final Tree<String> tree = new TreeImplInner<>("A", new TreeImplInner<>("B"), new TreeImplInner<>("C"));
		tree.setValue("C");
		assertEquals("Tree#setValue(E) => Tree contains branch with value 'C', therefore value is not replaced.", "A",
				tree.getValue());
	}

	@Test
	public void testAddBranchDuplicateValue() {
		final Tree<String> tree = new TreeImplInner<>("A", new TreeImplInner<>("B"), new TreeImplInner<>("C"));
		tree.addBranch(new TreeImplInner<>("B"));
		assertEquals("Tree.setValue(E) => Tree already contains branch with value 'B', therefore branch is not added.",
				2, tree.getBranches().length);
	}

	@Test
	public void testGetBranches() {
		assertEquals("Tree#getBranches() => Tree should be empty.", 0, new TreeImplInner<>("A").getBranches().length);
		final Tree<String> tree = new TreeImplInner<>("A", new TreeImplInner<>("B"), new TreeImplInner<>("C"));
		// 2 Leaves
		assertEquals("Tree#getBranches() => Tree should have 2 leaves.", 2, tree.getBranches().length);
		// Should contain B
		assertEquals("Tree#getBranches() => Tree's 1st leaf should be a tree with name 'B'.", "B",
				tree.getBranchByValue("B").getValue());
		// Should contain C
		assertEquals("Tree#getBranches() => Tree's 2nd leaf should be a tree with name 'C'.", "C",
				tree.getBranchByValue("C").getValue());
	}

	@Test
	public void testGetBranchByValue() {
		final String method = "Tree#getBranchByValueByValue(E) =>";
		final Tree<String> tree = new TreeImplInner<>("A");
		// Null parameter
		String msg = String.format("%s Method is expected to return 'null' when 'null' parameter is passed.", method);
		assertEquals(msg, null, tree.getBranchByValue(null));
		// No branches within trees
		msg = String.format("%s Method is expected to return 'null' there are no branches within tree.", method);
		assertEquals(msg, null, tree.getBranchByValue("B"));
		// Tree with Branches
		final Tree<String> tree2 = new TreeImplInner<>("A", new TreeImplInner<>("B"), new TreeImplInner<>("C"));
		// Branch present
		msg = String.format("%s Method is expected to return 'B'.", method);
		assertEquals(msg, new TreeImplInner<>("B").getValue(), tree2.getBranchByValue("B").getValue());
		// No branch with given tree-value
		msg = String.format("%s Method is expected to return 'null' since no branch with given value exists in tree.",
				method);
		assertEquals(msg, null, tree2.getBranchByValue("D"));
	}

	@Test
	public void testTreeValueList() {
		// .... A
		// ... /.\
		// ...B.. C
		// ....../.\
		// .....D...E
		final Tree<String> treeA = new TreeImplInner<>("A", new TreeImplInner<>("B"),
				new TreeImplInner<String>("C", new TreeImplInner<>("D"), new TreeImplInner<>("E")));
		final List<List<String>> treeValueSets = Arrays.asList(Arrays.asList("A", "B"), Arrays.asList("A", "C", "D"),
				Arrays.asList("A", "C", "E"));

		assertEquals("Tree#treeList() => Set of tree-value items must match with expected set.", treeValueSets,
				treeA.treeValueList()); // Expected -> [[A, B], [A, C, D], [A, C, E]]
	}

	@Test
	public void testContainsTree() {
		assertFalse("Tree#containsTree(Tree<E>) => The method 'containsTree' should return false.",
				new TreeImplInner<String>("A").containsTree(null));
	}

	@Test
	public void testIterator() {
		final List<String> expectedList = Arrays.asList("A", "B", "C", "D", "E");
		// Tree
		final Tree<String> treeA = new TreeImplInner<>("A", new TreeImplInner<>("B"),
				new TreeImplInner<String>("C", new TreeImplInner<>("D"), new TreeImplInner<>("E")));
		// Add iterator element to list
		final List<String> list = new ArrayList<>(5);
		treeA.iterator().forEachRemaining(list::add);
		// Compare List
		assertEquals("Tree#iterator() => Iterator items added to list, and list should be equal to the expectedList.",
				expectedList, list);
	}

	@Test
	public void testIteratorSingleItem() {
		final Tree<String> treeA = new TreeImplInner<>("A", new TreeImplInner<String>("E", new TreeImplInner<>("I")),
				new TreeImplInner<String>("O", new TreeImplInner<>("U")));
		final Iterator<String> itr = treeA.iterator();
		assertTrue("Tree#iterator() => Iterator must evaluate 'hasNext' method to true.", itr.hasNext());
		assertEquals("Tree#iterator() => Iterator#next() should return 'A'.", "A", itr.next());
		assertTrue("Tree#iterator() => Iterator must evaluate 'hasNext' method to true.", itr.hasNext());
		assertEquals("Tree#iterator() => Iterator#next() should return 'E'.", "E", itr.next());
		assertTrue("Tree#iterator() => Iterator must evaluate 'hasNext' method to true.", itr.hasNext());
		assertEquals("Tree#iterator() => Iterator#next() should return 'I'.", "I", itr.next());
		assertTrue("Tree#iterator() => Iterator must evaluate 'hasNext' method to true.", itr.hasNext());
		assertEquals("Tree#iterator() => Iterator#next() should return 'O'.", "O", itr.next());
		assertTrue("Tree#iterator() => Iterator must evaluate 'hasNext' method to true.", itr.hasNext());
		assertEquals("Tree#iterator() => Iterator#next() should return 'U'.", "U", itr.next());
		assertFalse("Tree#iterator() => Iterator must evaluate 'hasNext' method to false.", itr.hasNext());
	}

	/**
	 * Inner class for testing abstract class {@link AbstractTree}.
	 * 
	 * @author Vivek
	 * @param <E>
	 */
	private static class TreeImplInner<E> extends AbstractTree<E> {

		@SafeVarargs
		public TreeImplInner(final E value, Tree<E>... branches) {
			super(value, branches);
		}

		// This method is not meant to be tested for this inner class.
		@Override
		public boolean containsTree(Tree<E> searchTree) {
			return false;
		}
	}
}
