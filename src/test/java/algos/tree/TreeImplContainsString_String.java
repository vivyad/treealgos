package algos.tree;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TreeImplContainsString_String {

	@Test
	public void testcontainsTreeTrue() {
		String message;
		String format = "Tree#containsTree(Tree) => '%s' contains '%s'.";

		// .... A
		// ... /.\
		// ...B.. C
		// ....../.\
		// .....D...E
		final Tree<String> treeA = new TreeImplContainsString<>("A", new TreeImplContainsString<>("B"),
				new TreeImplContainsString<String>("C", new TreeImplContainsString<>("D"),
						new TreeImplContainsString<>("E")));

		// Tree[A] contains Tree[A]
		assertTrue("Tree#containsTree(Tree) => Tree always contains itself.", treeA.containsTree(treeA));

		// ....C
		// .../.\
		// ..D...E
		final Tree<String> treeC1 = new TreeImplContainsString<>("C", new TreeImplContainsString<>("D"),
				new TreeImplContainsString<>("E"));

		// Tree[A] contains Tree[C1]
		message = String.format(format, "A->B , A->C->D , A->C->E", "C->D , C->E");
		assertTrue(message, treeA.containsTree(treeC1));

		// ....C
		// .../
		// ..D
		final Tree<String> treeC2 = new TreeImplContainsString<>("C", new TreeImplContainsString<>("D"));

		// Tree[A] contains Tree[C2]
		message = String.format(format, "A->B , A->C->D , A->C->E", "C->D");
		assertTrue(message, treeA.containsTree(treeC2));

		// Tree[C1] contains Tree[C2]
		message = String.format(format, "C->D , C->E", "C->D");
		assertTrue(message, treeC1.containsTree(treeC2));

		// ....C
		// .../\.\
		// ..D..F.E
		final Tree<String> treeC3 = new TreeImplContainsString<>("C", new TreeImplContainsString<>("D"),
				new TreeImplContainsString<>("F"), new TreeImplContainsString<>("E"));

		// Tree[C3] contains Tree[C1]
		message = String.format(format, "C->D , C->F , C->E", "C->D , C->E");
		assertTrue(message, treeC3.containsTree(treeC1));

		// Tree[C3] contains Tree[C2]
		message = String.format(format, "C->D , C->F , C->E", "C->D");
		assertTrue(message, treeC3.containsTree(treeC2));

		// .... A
		// ... /.\
		// ...B.. C
		// ....../.\
		// .....E...D
		final Tree<String> treeA2 = new TreeImplContainsString<>("A", new TreeImplContainsString<>("B"),
				new TreeImplContainsString<String>("C", new TreeImplContainsString<>("E"),
						new TreeImplContainsString<>("D")));

		// Tree[A] contains Tree[A2]
		message = String.format(format, "A->B , A->C->D , A->C->E", "A->B , A->C->E , A->C->D");
		assertTrue(message, treeA.containsTree(treeA2));

		// ......A
		// ...../.\
		// ....B...C
		// .../.../\.\
		// ..G...D..E.F
		final Tree<String> treeA3 = new TreeImplContainsString<>("A",
				new TreeImplContainsString<String>("B", new TreeImplContainsString<>("G")),
				new TreeImplContainsString<String>("C", new TreeImplContainsString<>("D"),
						new TreeImplContainsString<>("E"), new TreeImplContainsString<>("F")));

		// Tree[A3] contains Tree[A]
		message = String.format(format, "A->B->G , A->C->D , A->C->E , A->C->F", "A->B , A->C->D , A->C->E");
		assertTrue(message, treeA3.containsTree(treeA));

		// Tree[A3] contains Tree[C3]
		message = String.format(format, "A->B->G , A->C->D , A->C->E , A->C->F", "C->D , C->F , C->E");
		assertTrue(message, treeA3.containsTree(treeC3));

		// .......A
		// ....../|\
		// ...../.|.\
		// ....B..H..C
		// .../...../\.\
		// ..G.....D..E.F
		final Tree<String> treeA4 = new TreeImplContainsString<>("A",
				new TreeImplContainsString<String>("B", new TreeImplContainsString<>("G")),
				new TreeImplContainsString<>("H"),
				new TreeImplContainsString<String>("C", new TreeImplContainsString<>("D"),
						new TreeImplContainsString<>("E"), new TreeImplContainsString<>("F")));

		// Tree[A4] contains Tree[A]
		message = String.format(format, "A->B->G , A->H , A->C->D , A->C->E , A->C->F", "A->B , A->C->D , A->C->E");
		assertTrue(message, treeA4.containsTree(treeA));

		// Tree[A4] contains Tree[A3]
		message = String.format(format, "A->B->G , A->H , A->C->D , A->C->E , A->C->F",
				"A->B->G , A->C->D , A->C->E , A->C->F");
		assertTrue(message, treeA4.containsTree(treeA3));

		// Tree[A4] contains Tree[C3]
		message = String.format(format, "A->B->G , A->H , A->C->D , A->C->E , A->C->F", "C->D , C->F , C->E");
		assertTrue(message, treeA4.containsTree(treeC3));
	}

	@Test
	public void testcontainsTreeFalse() {
		String format = "Tree#containsTree(Tree) => '%s' does not contains '%s'.";

		assertFalse("Tree#containsTree(Tree) => Tree cannot contain null.",
				new TreeImplContainsString<String>("A").containsTree(null));

		// .... A
		// ... /.\
		// ...B.. C
		// ....../.\
		// .....D...E
		final Tree<String> treeA = new TreeImplContainsString<>("A", new TreeImplContainsString<>("B"),
				new TreeImplContainsString<String>("C", new TreeImplContainsString<>("D"),
						new TreeImplContainsString<>("E")));

		// ....C
		// .../\.\
		// ..D..F.E
		final Tree<String> treeC3 = new TreeImplContainsString<>("C", new TreeImplContainsString<>("D"),
				new TreeImplContainsString<>("F"), new TreeImplContainsString<>("E"));

		// Tree[A] contains Tree[C3]
		String message = String.format(format, "A->B , A->C->D , A->C->E", "C->D , C->F , C->E");
		assertFalse(message, treeA.containsTree(treeC3));

		// .......A
		// ....../|\
		// ...../.|.\
		// ....B..H..C
		// .../...../\.\
		// ..G.....D..E.F
		final Tree<String> treeA4 = new TreeImplContainsString<>("A",
				new TreeImplContainsString<String>("B", new TreeImplContainsString<>("G")),
				new TreeImplContainsString<>("H"),
				new TreeImplContainsString<String>("C", new TreeImplContainsString<>("D"),
						new TreeImplContainsString<>("E"), new TreeImplContainsString<>("F")));

		// Tree[A4] contains Tree[A]
		message = String.format(format, "A->B , A->C->D , A->C->E", "A->B->G , A->H , A->C->D , A->C->E , A->C->F");
		assertFalse(message, treeA.containsTree(treeA4));
	}

}
