package algos.tree;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TreeImplTestInt {

	/**
	 * Test with INTEGER value
	 */
	@Test
	public void testcontainsTreeInt() {
		String message;
		String format = "Tree#containsTree(Tree) => '%s' contains '%s'.";

		// ....1
		// .../.\
		// ..10..11
		final Tree<Integer> tree1 = new TreeImpl<>(1, new TreeImpl<>(10),
				new TreeImpl<>(11));

		// ....1
		// .../
		// ..10
		final Tree<Integer> tree2 = new TreeImpl<>(1, new TreeImpl<>(10));

		// Tree[1] contains Tree[2]
		message = String.format(format, "1->10,1->11", "1->10");
		assertTrue(message, tree1.containsTree(tree2));

		// ......C
		// ...../|\
		// ..../.|.\
		// .../..|..\
		// ../...|...\
		// 10...12...11
		final Tree<Integer> tree3 = new TreeImpl<>(1, new TreeImpl<>(10),
				new TreeImpl<>(12), new TreeImpl<>(11));

		// Tree[3] contains Tree [2]
		message = String.format(format, "1->10,1->12,1->11", "1->10,1->11");
		assertTrue(message, tree3.containsTree(tree2));

		// Tree[2] contains Tree Tree[2]
		format = "Tree#containsTree(Tree) => '%s' does not contains '%s'.";
		message = String.format(format, "1->10,1->12,1->11", "1->10,1->11");
		assertFalse(message, tree2.containsTree(tree3));
	}

}
