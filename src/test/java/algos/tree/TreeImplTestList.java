package algos.tree;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class TreeImplTestList {

	@Test
	public void testcontainsTreeListTrue() {
		String message;
		String format = "Tree#containsTree(Tree) => '%s' contains '%s'.";

		// .......[1,2]
		// ....../....\
		// ..[11,12]...[21,22]
		final Tree<List<Integer>> tree1 = new TreeImplContainsString<>(Arrays.asList(1, 2),
				new TreeImplContainsString<>(Arrays.asList(11, 12)),
				new TreeImplContainsString<>(Arrays.asList(21, 22)));

		// ...[1,2]
		// .....|
		// ..[11,12]
		final Tree<List<Integer>> tree2 = new TreeImplContainsString<>(Arrays.asList(1, 2),
				new TreeImplContainsString<>(Arrays.asList(11, 12)));

		// Tree[1] contains Tree[2]
		message = String.format(format, "[1, 2]->[11, 12],[1, 2]->[21, 22]", "[1, 2]->[11, 12]");
		assertTrue(message, tree1.containsTree(tree2));

		// .........[ 1 , 2]
		// ......../.. |...\
		// ..[11,12].[19,20].[21,22]
		final Tree<List<Integer>> tree3 = new TreeImplContainsString<>(Arrays.asList(1, 2),
				new TreeImplContainsString<>(Arrays.asList(11, 12)), new TreeImplContainsString<List<Integer>>(
						Arrays.asList(19, 20), new TreeImplContainsString<>(Arrays.asList(21, 22))));

		// Tree[3] contains Tree[2]
		message = String.format(format, "[1, 2]->[11, 12],[1, 2]->[19, 20]->[21, 22]",
				"[1, 2]->[11, 12],[1, 2]->[21, 22]");
		assertTrue(message, tree3.containsTree(tree2));
	}

}
